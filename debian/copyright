Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MPD_sima
Upstream-Contact: http://kaliko.me/code/mpd-sima/
Source: <http://media.kaliko.me/src/sima/releases/>

Files: *
Copyright: Copyright 2009-2019 Jack Kaliko <kaliko@azylum.org>
           Copyright 2010 Eric Casteleijn <thisfred@gmail.com>
           Copyright 2007, 2009 Sander Marechal <s.marechal@jejik.com>
           Copyright 2008 Rick van Hattem
License: GPL-3+

Files: mpd-sima simadb_cli sima/* data/*
Copyright: Copyright 2009-2019 Jack Kaliko <kaliko@azylum.org>
License: GPL-3+

Files: sima/lib/daemon.py
Comment: Daemon code is meant to be public domain.
 Its original author stated so as a fallback or default for not having licenced
 its code and because of the few simple lines it was.
 The upstream author decided to use GPL for its modifications.
 http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/
Copyright: Copyright 2009-2015 Jack Kaliko <kaliko@azylum.org>
           Copyright 2007, 2009 Sander Marechal <s.marechal@jejik.com>
License: GPL-3+

Files: sima/lib/track.py sima/lib/simadb.py sima/utils/utils.py
Comment: The throttle decorator in utils.py is copyrighted to Eric Casteleijn.
 Rick van Hattem owns copyrights in simadb.py for database management.
Copyright: Copyright 2009-2019 Jack Kaliko <kaliko@azylum.org>
           Copyright 2010 Eric Casteleijn <thisfred@gmail.com>
           Copyright 2008 Rick van Hattem
License: GPL-3+

Files: debian/*
Copyright: Copyright 2010-2019 Geoffroy Youri Berret <efrim@azylum.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.
